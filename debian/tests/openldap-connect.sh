#!/bin/bash
set -e

echo "Starting test 1..."

### slapd install config
cat > debconf-slapd.conf << 'EOF'
slapd slapd/password1 password admin
slapd slapd/internal/adminpw password admin
slapd slapd/internal/generated_adminpw password admin
slapd slapd/password2 password admin
slapd slapd/unsafe_selfwrite_acl note
slapd slapd/purge_database boolean false
slapd slapd/domain string example.com
slapd slapd/ppolicy_schema_needs_update select abort installation
slapd slapd/invalid_config boolean true
slapd slapd/move_old_database boolean false
slapd slapd/backend select MDB
slapd shared/organization string ETH Brussels
slapd slapd/dump_database_destdir string /var/backups/slapd-VERSION
slapd slapd/no_configuration boolean false
slapd slapd/dump_database select when needed
slapd slapd/password_mismatch note
EOF
export DEBIAN_FRONTEND=noninteractive

### Install packages
cat debconf-slapd.conf | debconf-set-selections
dpkg-reconfigure slapd
service slapd restart
rm debconf-slapd.conf

# Just check if it exists.
ls /usr/share/doc/openvpn-auth-ldap/examples/auth-ldap.conf

### LDAP server config
cat > user.ldif << 'EOF'
dn: ou=people,dc=example,dc=com
objectClass: organizationalUnit
ou: people

dn: uid=john,ou=people,dc=example,dc=com
objectClass: inetOrgPerson
uid: john
sn: Doe
givenName: John
cn: John Doe
displayName: John Doe
mail: john@example.com
userPassword: johnldap
EOF
ldapadd -x -D 'cn=admin,dc=example,dc=com' -f user.ldif -w admin
rm user.ldif
ldapsearch -x -b "dc=example,dc=com"

### OpenVPN config
mkdir /etc/openvpn/auth

# chroot hack
cd /dev
mkdir net
cd net
mknod tun c 10 200
chmod 666 tun
cd

# easy-rsa
make-cadir easy-rsa/
export EASYRSA_BATCH="yes"
export EASYRSA_REQ_CN="Easy-RSA CA"
export KEY_COUNTRY="US"
export KEY_PROVINCE="CA"
export KEY_CITY="SanFrancisco"
export KEY_ORG="Fort-Funston"
export KEY_EMAIL="mail@domain"
cd easy-rsa/
./easyrsa clean-all
./easyrsa build-ca nopass
./easyrsa build-server-full server nopass
./easyrsa gen-dh
cp pki/private/server.key /etc/openvpn
cp pki/issued/server.crt /etc/openvpn
cp pki/ca.crt /etc/openvpn
cp pki/dh.pem /etc/openvpn

# LDAP plugin config
cat > /etc/openvpn/auth/auth-ldap.conf << 'EOF'
<LDAP>
        # LDAP server URL
        URL             ldap://localhost

        # Network timeout (in seconds)
        Timeout         15

        # Enable Start TLS
        TLSEnable       no

        # Follow LDAP Referrals (anonymously)
        FollowReferrals yes
</LDAP>

<Authorization>
        # Base DN
        BaseDN          "ou=people,dc=example,dc=com"

        # User Search Filter
        SearchFilter    "(&(uid=%u)(objectClass=inetOrgPerson))"

        # Require Group Membership
        RequireGroup    false
</Authorization>
EOF

# Server config
cat > /etc/openvpn/server.conf << 'EOF'
port 1194
proto tcp
dev tun
ca ca.crt
cert server.crt
key server.key  # This file should be kept secret
dh dh.pem
server 10.8.0.0 255.255.255.0
ifconfig-pool-persist ipp.txt
push "dhcp-option DNS 1.0.0.1"
push "dhcp-option DNS 208.67.220.220"
keepalive 10 120
cipher AES-256-CBC
user nobody
group nogroup
persist-key
persist-tun
explicit-exit-notify 0
plugin /usr/lib/openvpn/openvpn-auth-ldap.so /etc/openvpn/auth/auth-ldap.conf
verify-client-cert optional
status openvpn-status.log
username-as-common-name
log-append  /var/log/openvpn.log
verb 3
EOF

# Login config
cat > /etc/openvpn/login << 'EOF'
john
johnldap
EOF

# Client config
cat > /etc/openvpn/client.ovpn << 'EOF'
client
ca /etc/openvpn/ca.crt
remote localhost 1194
auth-user-pass /etc/openvpn/login
cipher AES-256-CBC
dev tun
proto tcp
nobind
auth-nocache
persist-key
persist-tun
user nobody
group nogroup
script-security 2
EOF
openvpn --daemon --cd /etc/openvpn --config /etc/openvpn/server.conf
echo "Server started, sleeping 3 seconds..."
sleep 3
openvpn --config /etc/openvpn/client.ovpn --daemon

# Wait 70 seconds for the connection and kill it.
sleep 70

# Check if john is connected
if grep -q john /etc/openvpn/openvpn-status.log; then
    echo "VPN connected."
    exit 0
else
    echo "User not connected to VPN."
    exit 1
fi
